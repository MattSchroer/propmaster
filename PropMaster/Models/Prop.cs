﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropMaster.Models
{
	public class Prop
	{
		public string GroupNumber { get; set; }

		public string ItemNumber { get; set; }

		public string ObjectType { get; set; }

		public string Color { get; set; }

		public string Material { get; set; }

		public string Description { get; set; }

		public string Category { get; set; }

		public string UsedFor { get; set; }

		public string OwnedBy { get; set; }

		public string ImagePath { get; set; }

		public string CheckedOutBy { get; set; }

		public string CurrentLocation { get; set; }

	}
}